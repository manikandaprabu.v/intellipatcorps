package baseTest;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import io.github.bonigarcia.wdm.WebDriverManager;
//Just for commitddd
public class BaseTest {
	
	 public static final String AUTOMATE_USERNAME = "manikandaprabu1";
	 public static final String AUTOMATE_ACCESS_KEY = "ICHzisDKHJqRVPNKAspH";
	 public static final String URL = "https://" + AUTOMATE_USERNAME + ":" + AUTOMATE_ACCESS_KEY + "@hub-cloud.browserstack.com/wd/hub";
	
	static WebDriver driver ;
	WebDriverWait wait;
	
	@BeforeTest
	public void initBrowser()
	{
		WebDriverManager.chromedriver().setup();
		URL driverUrl;
		try {
			DesiredCapabilities caps =new DesiredCapabilities();
		    caps.setCapability("os_version", "10");
		    caps.setCapability("resolution", "1920x1080");
		    caps.setCapability("browser", "Chrome");
		    caps.setCapability("browser_version", "latest");
		    caps.setCapability("browserStackLocal", "true");		    
			driver = new RemoteWebDriver(new URL(URL),caps);
		} catch (MalformedURLException e) {
			
			e.printStackTrace();
		}
		
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.manage().window().maximize();
		driver.navigate().to("https://opensource-demo.orangehrmlive.com");
	}
	
	@AfterTest
	public void tearDown()
	{
		driver.close();
	}
	

	public static WebDriver getDriver() {
		return driver;
	}
	

}

package keywordDriver;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import baseTest.BaseTest;
import seleniumActions.UserActions;
import utility.UsingFor;

public class ExecutingTestCases {
	
	WebDriver driver;
	UserActions actions;
	
	@Test(dataProvider = "keywordDriver")
	public void executeTestCases(String testCase,String testSteps,String keywords,String element,String testData)
	{
	driver = BaseTest.getDriver();
	actions= new UserActions(driver);

	switch(keywords)
	{
	case "navigate":
		actions.doNavigate(testData);
		break;
	case "lclick":
		actions.doLeftClick(element);
		break;
	case "type":
		actions.doType(element, testData);
		break;
	case "checkTitle":
		actions.doVerifyTitle(testData);
		break;
	case "checkUrl":
		actions.doVerifyURL(testData);
		break;
		default:
			System.out.println("Invalid keyword");
	}
		
	}
	
	
	
	@DataProvider(name="keywordDriver")
	public Object[][] readKeywords()
	{
		UsingFor testData = new UsingFor();
		return testData.getTestData("TestCase2");
		
	}

}

package pageFactory;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


import seleniumActions.SeleniumWaits;

public class DashboardPage {
	

	WebDriver driver;
	SeleniumWaits waits;
	public static final Logger log = LogManager.getLogger(DashboardPage.class);
	
	public DashboardPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	private String dashboardPageURL="dashboard";
	
	@FindBy(how=How.XPATH,using="//img[contains(@src,'ApplyLeave')]")
	private WebElement assignLeave;
	
	public boolean verifyUrl()
	{
		waits= new SeleniumWaits(driver);
		return waits.waitForURL(dashboardPageURL);
	}
	
	public void clickAssignLeave()
	{
		waits.waitForElementToClick(assignLeave);
		assignLeave.click();
	}

}

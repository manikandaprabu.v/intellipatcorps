package pageFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


import seleniumActions.SeleniumWaits;

public class HomePage {
	
	WebDriver driver;
	SeleniumWaits waits;
	public static final Logger log = LogManager.getLogger(HomePage.class);
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	private String homePageTitle="OrangeHRM";
	
	@FindBy(how=How.ID,using="txtUsername")
	private WebElement username;
	
	@FindBy(how=How.NAME,using="txtPassword")
	private WebElement password;
	
	@FindBy(how=How.XPATH,using="//input[@id='btnLogin']")
	private WebElement loginButton;
	
	@FindBy(how=How.PARTIAL_LINK_TEXT,using="Forgot")
	private WebElement forgotPwdLink;
	
	public boolean verifyTitle()
	{
		waits = new SeleniumWaits(driver);
		return waits.waitForTitle(homePageTitle);
		
	}
	
	public void enterUserName(String uname)
	{
		log.info("Entering text in :" +username.getAccessibleName());
		if(waits.waitForVisibilityOfElement(username)==true) {
			username.clear();
			username.sendKeys(uname);
		}else
		{
			log.error("Element is not available to perform operation");
		}	
		
	}
	
	public void enterPassword(String passwd)
	{
		if(waits.waitForVisibilityOfElement(password)==true) {
			password.clear();
			password.sendKeys(passwd);
		}else
		{
			log.error("Element is not available to perform operation");
		}	
		
	}
	
	public void clickLoginButton()
	{
		if(waits.waitForElementToClick(loginButton)==true)
		{
			loginButton.click();
		}else
		{
			log.error("Element is not available to perform operation");
		}	
	}

}

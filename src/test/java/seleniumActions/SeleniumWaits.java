package seleniumActions;

import java.time.Duration;

import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;

public class SeleniumWaits {
	
	public static final Logger log = Logger.getLogger(SeleniumWaits.class);
	
	WebDriver driver;
	WebDriverWait wait;
	
	public SeleniumWaits(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
	}
	
	public boolean waitForVisibilityOfElement(WebElement element)
	{
		boolean flag=false;
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			flag=true;
		}catch(ElementNotVisibleException e)
		{
			log.error(element.getAccessibleName()+":Element not visible after waiting for 10 seconds");
		}
		return flag;
	}
	
	public boolean waitForElementToClick(WebElement element)
	{
		boolean flag=false;
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
			flag=true;
		}catch(ElementNotInteractableException e)
		{
			log.error(element.getAccessibleName()+":Element not clickable after waiting for 10 seconds");
		}
		return flag;
	}
	
	public boolean waitForTitle(String title)
	{
		boolean flag=false;
		try {
			wait.until(ExpectedConditions.titleIs(title));
			flag=true;
		}catch(TimeoutException e)
		{
			log.error(title+": of the page doesnt matches with current page title :"+driver.getTitle());
		}
		return flag;
	}
	
	public boolean waitForURL(String url)
	{
		boolean flag=false;
		try {
			wait.until(ExpectedConditions.urlContains(url));
			flag=true;
		}catch(TimeoutException e)
		{
			log.error(url+": of the page doesnt matches with current page url :"+driver.getCurrentUrl());
		}
		return flag;
	}
	
	
	
	

}

package seleniumActions;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.ReadElementInfo;

public class UserActions {

	public static final Logger log = LogManager.getLogger(UserActions.class);
	
	WebDriver driver;
	WebDriverWait wait;
	
	public UserActions(WebDriver driver)
	{
		this.driver=driver;
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
	}
	
	public void doType(String elementName,String testData)
	{
		log.info("Entering the value to the" +elementName + "with" +testData);
		try {
			wait.until(ExpectedConditions.visibilityOf(ReadElementInfo.getElement(elementName, driver)));
		ReadElementInfo.getElement(elementName, driver).clear();
		ReadElementInfo.getElement(elementName, driver).sendKeys(testData);
		}catch(TimeoutException e)
		{
			log.error("Unable to view the element" +elementName);
			throw new NoSuchElementException("Element not visible");
		}
	}
	
	public void doLeftClick(String elementName)
	{
		log.info("Clicking the " +elementName);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(ReadElementInfo.getElement(elementName, driver)));
		ReadElementInfo.getElement(elementName, driver).click();
		}catch(TimeoutException e)
		{
			log.error("Unable to click the element" +elementName);
			throw new ElementClickInterceptedException("Element not clickable");
		}
	}
	
	public void doNavigate(String testData)
	{
		log.info("Navigate to Page" +testData);
		try {
			driver.navigate().to(new URL(testData));
		} catch (MalformedURLException e) {
			log.error("Unable to navigate to " +testData);
			throw new InvalidArgumentException("Invalid Url");
		}
	}
	
	public void doVerifyTitle(String title)
	{
		log.info("Verifying the title as" +title);
		try {
			wait.until(ExpectedConditions.titleIs(title));
		}catch(TimeoutException e)
		{
			log.error("Title dont match" +title);
			throw new InvalidArgumentException("Title doesnt matches");
		}
	}
	
	public void doVerifyURL(String url)
	{
		log.info("Verifying the url as" +url);
		try {
			wait.until(ExpectedConditions.urlContains(url));
		}catch(TimeoutException e)
		{
			log.error("URL dont match" +url);
			throw new InvalidArgumentException("URL doesnt matches");
		}
	}

}

package testSuites;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import baseTest.BaseTest;
import pageFactory.DashboardPage;
import pageFactory.HomePage;

public class RegressionCases {
	
	WebDriver driver;
	HomePage homePage;
	DashboardPage dashboardPage;
	
	@Test
	public void verifySuccessfulLogin()
	{
		driver = BaseTest.getDriver();
		homePage=new HomePage(driver);
		assertTrue(homePage.verifyTitle());
		homePage.enterUserName("Admin");
		homePage.enterPassword("admin123");
		homePage.clickLoginButton();
		dashboardPage= new DashboardPage(driver);
		assertTrue(dashboardPage.verifyUrl());
		dashboardPage.clickAssignLeave();		
	}

}

package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class UsingFor {
	
	public Object[][]  getTestData(String sheetName)
	{
		File excelLoc = new File(System.getProperty("user.dir")+"/src/test/resources/KeywordDriver.xlsx");
		Object [][] testData = null;
		try {
			FileInputStream fis = new FileInputStream(excelLoc);
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sh = wb.getSheet(sheetName);//sh.getSheetAt(0)
			int lastRow = sh.getLastRowNum();
			int lastCell = sh.getRow(0).getLastCellNum();
			testData = new Object[lastRow+1][lastCell];
			for(int i=0;i<=lastRow;i++)
			{
				for (int j=0;j<=lastCell;j++)
				{
					try {
					Cell cell = sh.getRow(i).getCell(j);
					if(cell.getCellType()==CellType.NUMERIC)
					{
						testData[i][j]=cell.getNumericCellValue();
						System.out.print(cell.getNumericCellValue());
					}
					else if(cell.getCellType()==CellType.STRING)
					{
						testData[i][j]=cell.getStringCellValue();
						System.out.print(cell.getStringCellValue());
					}
					else {
						testData[i][j]="n/a";
					}
					}catch(NullPointerException e)
					{
						System.err.print(e.getMessage());
					}
					System.out.print("\t");
				}
				System.out.println();
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return testData;
		
		
	}

}
